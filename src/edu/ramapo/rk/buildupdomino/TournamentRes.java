/**************************************************************
     * Name:  Rijesh Kumar                                      *
     * Project:  Project 3: BuildUp Domino - Android            *
     * Class:  OPL                                              *
     * Date:  11/15/2014                                        *
     ************************************************************/
package edu.ramapo.rk.buildupdomino;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class TournamentRes extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tournament_res);
		
		Intent intent=getIntent();
		//pd = (Parcel_Data)intent.getSerializableExtra("end_round");
		int compRoundWon = 0; 
		int humRoundWon = 0; 
		//intent.getIntExtra("compRoundWon", compRoundWon);
		//intent.getIntExtra("humRoundWon", humRoundWon);
		
		compRoundWon = Integer.parseInt(intent.getStringExtra("compRoundWon")) ;
		humRoundWon = Integer.parseInt(intent.getStringExtra("humRoundWon"));
		
		TextView textView = (TextView)findViewById(R.id.textView3);
		textView.setText(Integer.toString(compRoundWon) );
		
		textView = (TextView)findViewById(R.id.textView4);
		textView.setText(Integer.toString(humRoundWon) );
		
		String text = ""; 
		if (compRoundWon > humRoundWon) {
			text += "Computer"; 
		}
		else if (compRoundWon < humRoundWon) {
			text += "Human";
		}
		else {
			text += "Draw";
		}
		textView = (TextView)findViewById(R.id.TextView01);
		textView.setText(text);
		
	}
	
	public void exitApp (View view) {
		System.exit(1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tournament_res, menu);
		return true;
	}

}
