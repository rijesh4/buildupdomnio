/**************************************************************
     * Name:  Rijesh Kumar                                      *
     * Project:  Project 3: BuildUp Domino - Android            *
     * Class:  OPL                                              *
     * Date:  11/15/2014                                        *
     ************************************************************/
package edu.ramapo.rk.buildupdomino;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class Round_Winner extends Activity {
	private Parcel_Data pd = new Parcel_Data(); 
	private int compRoundWon = 0; 
	private int humRoundWon = 0;
	private SixSet humSet = new SixSet ('B'); 
	private SixSet compSet = new SixSet ('W');
	private Tile [] humBoneYard = humSet.retTiles();
	private Tile [] compBoneYard = compSet.retTiles();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_round__winner);
		Intent intent=getIntent();
		pd = (Parcel_Data)intent.getSerializableExtra("end_round"); 
		compRoundWon = pd.retCompRoundWon(); 
		humRoundWon = pd.retHumRoundWon(); 
		TextView textView = (TextView)findViewById(R.id.TextView01); 
		textView.setText(Integer.toString(pd.retHumScore()) ); 
		textView = (TextView)findViewById(R.id.TextView02);
		textView.setText(Integer.toString(pd.retCompScore()) );
		String winVal = ""; 
		if (pd.retCompScore() > pd.retHumScore()) {
			winVal += "Computer";
			compRoundWon++;
			pd.SetCompRoundWon(compRoundWon);
		}
		else if (pd.retCompScore() < pd.retHumScore()) {
			winVal += "Human";
			humRoundWon++;
			pd.SetHumRoundWon(humRoundWon); 
		}
		else {
			winVal += "Draw";
		}
		textView = (TextView)findViewById(R.id.TextView04);
		textView.setText(winVal );
		
		
	}
	public void initNewRound (View view) {
		pd.resetValues(); 
		pd.SetCompBoneYard(compBoneYard);
		pd.SetHumBoneYard(humBoneYard);
		pd.SetCompRoundWon(compRoundWon);
		pd.SetHumRoundWon(humRoundWon);
		//
		Intent intent = new Intent (this, Shuffle_Turn.class);
		intent.putExtra("to_shuffle_screen", pd); 
		startActivity(intent); 
		System.exit(1);
		
	}
	
	public void showTournamentRes (View view) {
		Intent intent = new Intent (this, TournamentRes.class);
		intent.putExtra("compRoundWon", Integer.toString(pd.retCompRoundWon())); 
		intent.putExtra("humRoundWon", Integer.toString(pd.retHumRoundWon()));
		startActivity(intent); 
		System.exit(1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.round__winner, menu);
		return true;
	}

}
