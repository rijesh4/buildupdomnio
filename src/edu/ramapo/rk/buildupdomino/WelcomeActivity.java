/**************************************************************
     * Name:  Rijesh Kumar                                      *
     * Project:  Project 3: BuildUp Domino - Android            *
     * Class:  OPL                                              *
     * Date:  11/15/2014                                        *
     ************************************************************/

package edu.ramapo.rk.buildupdomino;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

public class WelcomeActivity extends Activity {
	private Parcel_Data pd = new Parcel_Data();
	static final int READ_BLOCK_SIZE = 100;
	private SixSet humSet = new SixSet ('B'); 
	private SixSet compSet = new SixSet ('W');
	private Tile [] humBoneYard = humSet.retTiles();
	private Tile [] compBoneYard = compSet.retTiles();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome); 
	}
	
	
	public void initNewGame (View view) {
		
		pd.resetValues(); 
		pd.SetCompBoneYard(compBoneYard);
		pd.SetHumBoneYard(humBoneYard);
		
		Intent intent = new Intent (this, Shuffle_Turn.class);
		intent.putExtra("to_shuffle_screen", pd); 
		startActivity(intent); 
		System.exit(1);		
	}
	
	/** 
	 * Function Name: initLoadGame
	   Purpose: Load from an existing file to resume the game 
       Parameters:  
	   Return Value: void
	   Local Variables:
	   		String fileName: takes the file name to load from
Algorithm: 
	1) Gets line from the file , using the while loop to stop
	2) Checks for various types : ex. Hand, Stack, BoneYard using Regular Expression
	3) Based on the type it updates the tiles to the players object.
	4) Creates an Intent to start the game 
 Assistance Received: NONE 
	 * 
	 * 
	   Load game from existing file
	   @param 
	   @return 
	   */
	public void initLoadGame (View view) {
		String fileName = ""; 
		
		switch( view.getId() ){
		case R.id.b_loadGame:
			fileName = "myFile.txt"; 
			break;
		case R.id.b_loadGame2:
			fileName = "myFile1.txt";
			break;
		case R.id.b_loadGame3:
			fileName = "Fifth.txt";
			break;
		}
		
		boolean isHumanBlock = false; 
		Tile[] tileList = new Tile [28];
		for (int i = 0 ; i <28 ; i++ ) {
			tileList[i] = new Tile(); 
		}
		
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,fileName);
		//Read text from file
		StringBuilder text = new StringBuilder();
		try {
		    BufferedReader br = new BufferedReader(new FileReader(file));
		    String line;
		    String newLine = "" ; 
		    while ((line = br.readLine()) != null) {
		    	int count = 0; 
		        text.append(line);
		        text.append('\n');
		        
		        Pattern pattern = Pattern.compile("\\b\\w\\d\\d\\b");
		        Matcher matcher = pattern.matcher(line);
		        while (matcher.find()) {
		            newLine = matcher.group();
		            tileList[count++] = toTile(newLine);  
		          }
		        
		        pattern = Pattern.compile("Computer:");
		        matcher = pattern.matcher(line);
		        if (matcher.find()) {
		        	isHumanBlock = false; 
		        }
		        pattern = Pattern.compile("Human:");
		        matcher = pattern.matcher(line);
		        if (matcher.find()) {
		        	isHumanBlock = true; 
		        }
		        
		        pattern = Pattern.compile("Score:");
		        matcher = pattern.matcher(line);
		        if (matcher.find()) {
		        	pattern = Pattern.compile("\\d+");
			        matcher = pattern.matcher(line);
			        if (matcher.find()){
			        	if (isHumanBlock) {
			        		pd.SetHumScore( Integer.parseInt(matcher.group()));
			        	}
			        	else {
			        		pd.SetCompScore( Integer.parseInt(matcher.group()));
			        	}
			        }
		        }
		        pattern = Pattern.compile("Rounds Won:");
		        matcher = pattern.matcher(line);
		        if (matcher.find()) {
		        	pattern = Pattern.compile("\\d+");
			        matcher = pattern.matcher(line);
			        if (matcher.find()){
			        	if (isHumanBlock) {
			        		pd.SetHumRoundWon( Integer.parseInt(matcher.group()));
			        	}
			        	else {
			        		pd.SetCompRoundWon( Integer.parseInt(matcher.group()));
			        	}
			        }
		        }
		        pattern = Pattern.compile("Turn:");
		        matcher = pattern.matcher(line);
		        if (matcher.find()) {
		        	pattern = Pattern.compile("Computer");
			        matcher = pattern.matcher(line);
			        if (matcher.find()){
			        	pd.SetHumTurn(false); 
			        }
			        pattern = Pattern.compile("Human");
			        matcher = pattern.matcher(line);
			        if (matcher.find()){
			        	pd.SetHumTurn(true); 
			        }
		        }
		        
		        pattern = Pattern.compile("Stacks:");
		        matcher = pattern.matcher(line);
		        if (matcher.find()) {
		        	Tile [] temp = new Tile[6]; 
		        	for (int i = 0 ; i < 6 ; i++) {
		        		temp[i] = tileList[i]; 
		        	}
		        	
		        	if (isHumanBlock) {
		        		pd.SetHumStack(temp); 
		        	} 
		        	else {
		        		pd.SetCompStack(temp); 
		        	}
		        }
		        pattern = Pattern.compile("Hand:");
		        matcher = pattern.matcher(line);
		        if (matcher.find()) {
		        	Tile [] temp = new Tile[6]; 
		        	for (int i = 0 ; i < 6 ; i++) {
		        		temp[i] = tileList[i]; 
		        	}
		        	
		        	if (isHumanBlock) {
		        		pd.SetHumHand(temp); 
		        	} 
		        	else {
		        		pd.SetCompHand(temp); 
		        	}
		        }
		        pattern = Pattern.compile("Boneyard:");
		        matcher = pattern.matcher(line);
		        if (matcher.find()) {
		        	Tile [] temp = new Tile[28]; 
		        	for (int i = 0 ; i < 28 ; i++) {
		        		temp[i] = tileList[i]; 
		        	}
		        	if (!isHumanBlock) {
		        		pd.SetCompBoneYard(temp); 
		        	} 
		        	else {
		        		pd.SetHumBoneYard(temp); 
		        	}
		        }
		        line = "";
		        for (int i = 0 ; i <28 ; i++ ) {
					tileList[i] = new Tile(); 
				} 
		    }
		    br.close();
		}
		catch (IOException e) {
		    //You'll need to add proper error handling here
		}
		//Set HandCounter, handNum.
		int val = listCounter(pd.retCompBoneYard()); 
		switch (val/4) {
		case 7: 
			pd.SetHandNum(1); 
			break;
		case 5: 
			pd.SetHandNum(1); 
			break;
		case 4: 
			pd.SetHandNum(1); 
			break;	
		case 2:
			pd.SetHandNum(2); 
			break; 
		case 1:
			pd.SetHandNum(3); 
			break;
		case 0:
			pd.SetHandNum(4); 
			break;
		default:
			pd.SetHandNum(5); 
			break;
		}
		pd.SetCompHandCounter(listCounter(pd.retCompHand()));
		pd.SetHumHandCounter(listCounter(pd.retHumHand()));
		// Intent
		if (listCounter(pd.retHumHand()) == 0) {
			Intent intent = new Intent (this, Shuffle_Turn.class);
			intent.putExtra("to_shuffle_screen", pd); 
			startActivity(intent);  
			System.exit(1);		
			return;
		}
		else {
			Intent intent = new Intent (this, Init_Game.class);
			intent.putExtra("pre_screen_data", pd); 
			startActivity(intent); 
			System.exit(1);
		}	
	}
	/** 
	   To get the number of tiles, excluding the empty ones, in the List
	   @param Tile[] aTile, list of tiles.
	   @return count - number of tiles in the list.
	   */
	public int listCounter (Tile[] aTile) {
		int count = 0; 
		Tile temp = new Tile (); 
		while (!aTile[count].equals(temp)) {
			count++; 
			if (count >= aTile.length) {
				break; 
			}
		}
		return count; 
	}
	/** 
	   To convert a String to Tile
	   @param Tile stTile, the string to be converted.
	   @return Tile temp.
	   */
	public Tile toTile (String tileSt) {
		Tile temp = new Tile(); 
		temp.color =  tileSt.charAt(0);
		temp.left = Character.getNumericValue(tileSt.charAt(1));  
		temp.right = Character.getNumericValue(tileSt.charAt(2));
		temp.sum = temp.left + temp.right; 
		return temp; 
	}
	/** 
	   Initiate new Game Screen
	   @param 
	   @return 
	   */
}
