/**************************************************************
     * Name:  Rijesh Kumar                                      *
     * Project:  Project 3: BuildUp Domino - Android            *
     * Class:  OPL                                              *
     * Date:  11/15/2014                                        *
     ************************************************************/
package edu.ramapo.rk.buildupdomino;
import java.util.ArrayList;

/** 
 * Class name : compPlayer
   Purpose: To create basic compPlayer functionalities 
   Local Variables: 
Assistance Received: NONE 
 * 
 * 
   @param 
   @return 
   */
public class CompPlayer extends Player{
	private int compHumanStackLastCount = 0; 
	private int compHumanStackSeparateCount = 0; 
	private ArrayList toStack = new ArrayList();
	private ArrayList fromStack = new ArrayList();
	private boolean oppFlag = false; 
	
	public CompPlayer (Parcel_Data pd) {
		boneYard = pd.retCompBoneYard();  
		hand = pd.retCompHand(); 
		stack = pd.retCompStack(); 
		//isHumanTurn = pd.retHumTurn();
		handCounter = pd.retCompHandCounter(); 
		//oppStack = pd.retHumStack(); 
	}
	
	
	/** 
	   Call respective functions to initiate comp move alg. 
	   @param 
	   @return 
	   */
	public boolean compNextMove(Tile[] oppStack, Tile[] oppHand,  boolean oppFlag) {
		findAllViableMoves(oppStack, oppHand, oppFlag); 
		if (findMostEfficientMove()) {
			return true; 
		}
		else {
			return false; 
		}
		//findMostEfficientMove(); 
	}
	
	
	/** 
	 * Function Name: FindAllViableMoves
Purpose: Creates a stack of all possible moves which a computer can make    
Parameters: 
		Tiles* humStack- Takes Opponent stact 
		bool oppFlag - To make sure who is the opponent (useful in case of hint)
Return Value: bool - VOID
Local Variables:
	NONE
Algorithm: 
	1) take a tile from computer hand
	2) check if it is double -> if can place on any non double opp stack. 
		put in the list
	3) if non - double go through individually to check if the move will
		be valid.
Assistance Received: NONE
	   To replace a Tile from the hand, when game is running. 
	   @param 
	   @return 
	   */
	public void findAllViableMoves (Tile[] oppStack, Tile[] oppHand,  boolean oppFlag) {
		int count = -1; 
		boolean isDouble;  
		compHumanStackLastCount = 0; 
		compHumanStackSeparateCount = 0; 
		this.oppFlag = oppFlag;
		Tile checkTempTile = new Tile (); 
		
		Tile tempStack[] = new Tile[6];
		Tile tempHand[] = new Tile[6];
		for (int i = 0 ; i < 6 ; i++) { //Initialize temp variables
			tempStack[i] = new Tile();
			tempHand[i]  = new Tile(); 
		}
		
		if (oppFlag) { // Swaps cur PLayer and Opponents stack
			tempStack = stack; 
			stack = oppStack; 
			oppStack = tempStack; 
			//
			tempHand = hand; 
			hand = oppHand; 
			oppHand = tempHand;  
		}
		//
		while (++count < handCounter) {
			isDouble = false; 
			if (hand[count].left == hand[count].right) { 
				isDouble = true;
			}
			if (hand[count].equals(checkTempTile) ) {
				continue;
			}
			
			//Own Stack choose
			for (int i = 0; i < 6; i++) {
				if (isDouble) {
					
					if (stack[i].equals(checkTempTile) ) {
						continue;
					}
					
					if (!oppFlag ?(stack[i].color == 'W') : (stack[i].color == 'B')) { //Need to change this condition to accomodate 'Help' function
						continue; 
					}
			
					// If humStack Tile is a double
					if (stack[i].left == stack[i].right) {
						if (stack[i].sum <= hand[count].sum) {
							toStack.add(stack[i]); 
							fromStack.add(hand[count]);
							compHumanStackSeparateCount++; 
						}
						else { continue; }
					}
					else {
						toStack.add(stack[i]);
						fromStack.add(hand[count]);
						compHumanStackSeparateCount++;
						continue; 
					}
				}
				//If compHand Tile is not a double
				else {
					if (!oppFlag ?(oppStack[i].color == 'W') : (stack[i].color == 'B')) { //Need to change this condition to accomodate 'Help' function
						continue; 
					}
				
					if (stack[i].sum <= hand[count].sum) {
						toStack.add(stack[i]);
						fromStack.add(hand[count]);
						compHumanStackSeparateCount++;
					}
					else { continue; }
				}
			}
		}//end of while loop
		
		count = -1; 
		compHumanStackLastCount = compHumanStackSeparateCount;
		while (++count < handCounter) {
			isDouble = false; 
			if (hand[count].left == hand[count].right) { 
				isDouble = true;
			} 
			if (hand[count].equals(checkTempTile) ) {
				continue;
			}
			//Opponent Stack Choose
			for (int i = 0; i < 6; i++) {
				//
				if (oppStack[i].equals(checkTempTile) ) {
					continue;
				}
				if (!oppFlag ?(oppStack[i].color == 'W') : (oppStack[i].color == 'B')) { //Need to change this condition to accomodate 'Help' function
					continue; 
				}
		
				//
				if (isDouble) {
					// If compStack Tile is a double
					if (oppStack[i].left == oppStack[i].right) {
						if (oppStack[i].sum <= hand[count].sum) {
							toStack.add(oppStack[i]);
							fromStack.add(hand[count]);
							compHumanStackLastCount++;
						}
						else { continue; }
					}
					else {
						toStack.add(oppStack[i]);
						fromStack.add(hand[count]);
						compHumanStackLastCount++;
						continue;
					}
				}
				//If compHand Tile is not a double
				else {
				
					if (!oppFlag ?(oppStack[i].color == 'W') : (oppStack[i].color == 'B')) { //Need to change this condition to accomodate 'Help' function
						continue; 
					}
					if (oppStack[i].sum <= hand[count].sum) {
						toStack.add(oppStack[i]);
						fromStack.add(hand[count]);
						compHumanStackLastCount++;
					}
					else { continue; }
				}
			}
		}//end of while loop
		if (oppFlag) {
			stack = tempStack; 
			hand = tempHand; 
		}
			
	}//end of function
	
	
	
	/** 
	 * Function Name: findMostEfficientMove 
Purpose: From the list of all viable moves choose the most efficient one.    
Parameters: 
		bool oppFlag - If oppenent needs hint . Set
Return Value: bool - If a tile was chosen
Local Variables:
	NONE
Algorithm: 
	1) If tile is a double: find the greatest difference with oppTile 
	2) If tile is a non double : find the least difference. 
Assistance Received: NONE
	   find most efficient move from the list of all possible moves 
	   @param 
	   @return 
	   */	
	
	public boolean findMostEfficientMove () {
		int leastDifference = 13; // Because the greatest difference can be 12
		int greatestDifference = 0; 
		Tile left = new Tile(), right = new Tile(), lDiffLeftTile = new Tile(), lDiffRightTile = new Tile(), gDiffLeftTile = new Tile(), gDiffRightTile = new Tile(), temp = new Tile();
		compHumanStackSeparateCount = compHumanStackLastCount - compHumanStackSeparateCount;
		//
		if (compHumanStackLastCount < 1 ) {// In case there are no viable moves -> pass
			//cout<<" passed it Turn "<<endl; 
			pass = true; 
			return false; 
		}
	
		for (int i = 0; i < compHumanStackLastCount; i++) {
			right = (Tile)toStack.get(0); 
			toStack.remove(0); 
			left = (Tile)fromStack.get(0); 
			fromStack.remove(0); 
			if (!oppFlag ? (right.color == 'W') : (right.color == 'B')) { //Need to change this condition to accomodate 'Help' function
					continue; 
			}
			if (left.equals(temp) || right.equals(temp)) {
				continue;
			}
			if ((left.sum - right.sum) < leastDifference && (left.sum - right.sum) >= 0  ) {
				leastDifference = (left.sum - right.sum);
				lDiffLeftTile = left; 
				lDiffRightTile = right; 
			}
			if (left.left == left.right) {//player Tile isDouble
				if (( right.sum - left.sum ) > greatestDifference){
					greatestDifference = ( right.sum - left.sum);
					gDiffLeftTile = left;
					gDiffRightTile = right;
				}
			}
		}//end of for loop

		//Check here if choose greatest/least difference
		if (leastDifference <= 3) { // Other smaller tile
			if (lDiffLeftTile.left != lDiffRightTile.right ) {
				chosenTile = lDiffLeftTile;
				chosenStack = lDiffRightTile;
				return true;
			}
		}
		if (greatestDifference > 3 ) {// Placement of double
			chosenTile = gDiffLeftTile;
			chosenStack = gDiffRightTile;
			return true; 
		}
		else {
			chosenTile = lDiffLeftTile;
			chosenStack = lDiffRightTile;
			return true;
		}
		
	}
	
}
