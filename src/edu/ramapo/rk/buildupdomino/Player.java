/**************************************************************
     * Name:  Rijesh Kumar                                      *
     * Project:  Project 3: BuildUp Domino - Android            *
     * Class:  OPL                                              *
     * Date:  11/15/2014                                        *
     ************************************************************/
package edu.ramapo.rk.buildupdomino;

/** 
 * Class name : Player
   Purpose: To create basic player functionalities 
   Local Variables: 
Assistance Received: NONE 
 * 
 * 
   @param 
   @return 
   */
public class Player {
	protected Tile[] boneYard = new Tile [16];
	protected Tile[]  hand = new Tile [6];
	protected Tile[] stack = new Tile [6];
	protected Tile chosenTile = new Tile(); 
	protected Tile chosenStack = new Tile();
	protected int handCounter = 6; 
	protected boolean pass = false; 
	
	
	/** 
	   To replace a Tile from the hand, when game is running. 
	   @param 
	   @return 
	   */
	public void ReplaceTiles () {
		int i; 
		int count = findHandTileLoc();
		Tile[] temp = new Tile [6]; // 6 is constant because Update screen data asks for it
		for (int i1 = 0 ; i1 < 6 ; i1++) {
			temp[i1] = new Tile();	
		}
		//
		for (int k = 0 ; k < count ;k++ ) {
			temp[k] = hand[k]; 
		}
		for ( i  = count ; i < handCounter - 1; i++){
			temp[i] = hand[i+1]; 
		}
		hand = temp; 
		temp[i] = new Tile(); 
		handCounter--; 
		 
	}
	/** 
	   Find the location of a tile in Hand 
	   @param 
	   @return 
	   */	
	public int findHandTileLoc () {
		int count = 0; 
		for (int i = 0 ; i < handCounter; i++) {
			if (hand[i].equals(chosenTile)) {
				count = i; 
				break;
			}
		}
		return count;
	}
	
	//Getters and Setters
	public int retHandCounter () {
		return handCounter; 
	}
	public void setHandCounter(int val) {
		handCounter = val; 
	}
	
	public Tile[] retBoneYard () {
		return boneYard; 
	}
	
	public Tile retBoneyardInd (int val) {
		return boneYard[val-1]; 
	}
	public Tile retHandInd (int val) {
		return hand[val-1]; 
	}
	public Tile retStackInd (int val) {
		return stack[val-1]; 
	}
	public Tile[] retStack () {
		return stack; 
	}
	public Tile[] retHand () {
		return hand; 
	}
	public void setStackInd (Tile aTile, int val) {
		stack[val-1] = aTile; 
	}
	
	
	public void setChosenTile (Tile aTile) {
		Tile temp = new Tile(); 
		if (aTile.equals(temp)) {
			return; 
		}
		chosenTile = aTile; 
	}
	public Tile retChosenTile () {
		return chosenTile; 
	}
	public void setChosenStack (Tile aTile) {
		Tile temp = new Tile(); 
		if (aTile.equals(temp)) {
			return; 
		}
		chosenStack = aTile; 
	}
	public Tile retChosenStack () {
		return chosenStack; 
	}
	
	public String toStringList (Tile[] aTile) {
		int count = 0; 
		Tile temp = new Tile (); 
		String stTemp= "";
		while (!aTile[count].equals(temp)) {
			stTemp += aTile[count].color + Integer.toString(aTile[count].left) + Integer.toString(aTile[count].right) + " ";
			count++; 
			if (count >= aTile.length) {
				break; 
			}
		}
		return stTemp;
	}
	
	public String toString (Tile aTile) {
		if (aTile.color == 'R' || aTile.left == -1  || aTile.right == -2 || aTile == null) {
			return ""; 
		}
		String temp= "";
		temp += aTile.color + Integer.toString(aTile.left) + Integer.toString(aTile.right); 
		return temp;
	}
	
	
}
