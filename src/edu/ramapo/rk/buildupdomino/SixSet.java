/**
 * 
 */
package edu.ramapo.rk.buildupdomino;
import java.util.Random;

/**
 * @author Rijesh
 *
 */
public class SixSet  {
	/**
	 * 
	 */
	//private static final long serialVersionUID = 1677353115093503609L;
	private Tile[] tiles = new Tile [28];
	
	public SixSet (char tileCol) {
		int i = 0 ; 
		int temp = 0 ; // to make the second loop decrement after each completed loop
		 
		for (int j = 0 ; j <= 6; j++) {
			for (int k = 6; k>= temp; k--) {
				Tile tempTile = new Tile (j, k, j+k, tileCol);
				tiles[i++] = tempTile; 
				}
			temp++; 
		}		
	}
	
	public void Shuffle () {
		Random rand = new Random (); 
		//int shuffleCount = rand.nextInt(4);
		int shuffleLoc = 0; 
		for (int i = 0; i < 28 ; i++ ) {
			shuffleLoc = rand.nextInt(28); 
			Tile temp = tiles[i]; 
			tiles[i] = tiles[shuffleLoc]; 
			tiles[shuffleLoc] = temp; 
		}
	}
	
	public Tile[]  retTiles () {
		Shuffle(); 
		//return "hey bud ";
		return tiles; 
	}
}
