package edu.ramapo.rk.buildupdomino;

import java.io.Serializable;

public class Tile implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2193132817369046152L;
	public int left;
	public int right; 
	public int sum; 
	public char color; 
	
	public Tile () {
		left = -1; 
		right = -2; 
		sum = left + right;
		color = 'R';
	}
	public Tile (int left, int right, int sum, char col) {
		this.left = left; 
		this.right = right; 
		this.sum = sum; 
		this.color = col; 
	}
	
	public boolean equals (Tile other) {
		return (this.color == other.color && this.left == other.left && this.right == other.right );  
	}
	
}
