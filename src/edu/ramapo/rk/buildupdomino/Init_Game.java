/**************************************************************
     * Name:  Rijesh Kumar                                      *
     * Project:  Project 3: BuildUp Domino - Android            *
     * Class:  OPL                                              *
     * Date:  11/15/2014                                        *
     ************************************************************/

package edu.ramapo.rk.buildupdomino;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Init_Game extends Activity {
	private Parcel_Data pd = new Parcel_Data(); 
	private HumPlayer humPlayer;
	private CompPlayer compPlayer; 
	private boolean hasChosenHandTile = false; 
	private boolean isHumanTurn = false; 
	private boolean compPass = false;
	private boolean humPass = false;
	private boolean screenChangeSet = false; 
	//static final int READ_BLOCK_SIZE = 100;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_init__game);
		
		Intent intent=getIntent();
		//Tile[] temp = new Tile [6];  
		//Parcel_Data s1 = new Parcel_Data(); 
		pd = (Parcel_Data)intent.getSerializableExtra("pre_screen_data"); 
		humPlayer = new HumPlayer (pd);  
		compPlayer = new CompPlayer(pd);
		isHumanTurn = pd.retHumTurn();  
		if (!isHumanTurn) {
			initCompMove(); 
			isHumanTurn = true; 
		}
		//int x = pd.retHandNum(); 
		// if handNum >....
		
		Update_screen_data (); 
		
		
		
	}
/** 
	 * Function Name: respond
	   Purpose: respond to the user buttons pressed (Tiles in hands/stacks) 
       Parameters:  
	   Return Value: void
	   Local Variables:
	   		
Algorithm: 
	1) Check if handTile and Stacktile are chosen.
	2)  If chosen commit to move the tiles to new loc. 
 Assistance Received: NONE 
	 * 
	 * 
	   respond to the user buttons pressed (Tiles in hands/stacks)
	   @param 
	   @return 
	   */
	public void respond (View view) {
		boolean isInOppStack = false; 
		int stackCount = 0; 
		TextView textView = (TextView) findViewById(R.id.t_note); 
		textView.setText("Note");
		
		switch( view.getId() )
		{
		case R.id.humHand1: 
			humPlayer.setChosenTile(toTile(((Button) findViewById( R.id.humHand1 )).getText().toString()));	
			((Button) findViewById( R.id.humHand1 )).getBackground().setColorFilter(0xFF0000FF, PorterDuff.Mode.MULTIPLY);
			hasChosenHandTile = true; 
		break;
		case R.id.humHand2: 
			humPlayer.setChosenTile(toTile(((Button) findViewById( R.id.humHand2 )).getText().toString()));	
			((Button) findViewById( R.id.humHand2 )).getBackground().setColorFilter(0xFF0000FF, PorterDuff.Mode.MULTIPLY);
			hasChosenHandTile = true;
		break;
		case R.id.humHand3: 
			humPlayer.setChosenTile(toTile(((Button) findViewById( R.id.humHand3 )).getText().toString()));	
			((Button) findViewById( R.id.humHand3 )).getBackground().setColorFilter(0xFF0000FF, PorterDuff.Mode.MULTIPLY);
			hasChosenHandTile = true;
		break;
		case R.id.humHand4: 
			humPlayer.setChosenTile(toTile(((Button) findViewById( R.id.humHand4 )).getText().toString()));	
			((Button) findViewById( R.id.humHand4 )).getBackground().setColorFilter(0xFF0000FF, PorterDuff.Mode.MULTIPLY);
			hasChosenHandTile = true;
		break;
		case R.id.humHand5: 
			humPlayer.setChosenTile(toTile(((Button) findViewById( R.id.humHand5 )).getText().toString()));	
			((Button) findViewById( R.id.humHand5 )).getBackground().setColorFilter(0xFF0000FF, PorterDuff.Mode.MULTIPLY);
			hasChosenHandTile = true;
		break;
		case R.id.humHand6: 
			humPlayer.setChosenTile(toTile(((Button) findViewById( R.id.humHand6 )).getText().toString()));	
			((Button) findViewById( R.id.humHand6 )).getBackground().setColorFilter(0xFF0000FF, PorterDuff.Mode.MULTIPLY);
			hasChosenHandTile = true;
		break;
		//
		case R.id.compStack1:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.compStack1 )).getText().toString()));
				isInOppStack = true; 
				stackCount = 1; 
			}
			else {
				
			}
		break;
		case R.id.compStack2:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.compStack2 )).getText().toString()));
				isInOppStack = true; 
				stackCount = 2; 
			}
			else {
				
			}
		break;
		case R.id.compStack3:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.compStack3 )).getText().toString()));
				isInOppStack = true;
				stackCount = 3; 
			}
			else {
				
			}
		break;
		case R.id.compStack4:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.compStack4 )).getText().toString()));
				isInOppStack = true;
				stackCount = 4; 
			}
			else {
				
			}
		break;
		case R.id.compStack5:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.compStack5 )).getText().toString()));
				isInOppStack = true;
				stackCount = 5; 
			}
			else {
				
			}
		break;
		case R.id.compStack6:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.compStack6 )).getText().toString()));
				isInOppStack = true;
				stackCount = 6; 
			}
			else {
				
			}
		break;
		//
		case R.id.humStack1:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.humStack1 )).getText().toString()));
				stackCount = 1; 
			}
			else {
				
			}
		break;
		case R.id.humStack2:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.humStack2 )).getText().toString()));
				stackCount = 2; 
			}
			else {
				
			}
		break;
		case R.id.humStack3:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.humStack3 )).getText().toString()));
				stackCount = 3; 
			}
			else {
				
			}
		break;
		case R.id.humStack4:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.humStack4 )).getText().toString()));
				stackCount = 4; 
			}
			else {
				
			}
		break;
		case R.id.humStack5:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.humStack5 )).getText().toString()));
				stackCount = 5; 
			}
			else {
				
			}
		break;
		case R.id.humStack6:
			if (hasChosenHandTile) {
				humPlayer.setChosenStack(toTile(((Button) findViewById( R.id.humStack6 )).getText().toString()));
				stackCount = 6; 
			}
			else {
				
			}
		break;
		}// end of switch
		
		if (hasChosenHandTile && stackCount != 0) {
			if (humPlayer.ValidateMove()) {
				isHumanTurn = false;
				compPass = false; 
				humPlayer.ReplaceTiles(); 
				//find the count of the stack where the tile was chosen
				if (isInOppStack) {
					compPlayer.setStackInd(humPlayer.retChosenTile(), stackCount);
					hasChosenHandTile = false; 
					stackCount = 0 ; 	
				}
				else {
					humPlayer.setStackInd(humPlayer.retChosenTile(), stackCount);
					hasChosenHandTile = false; 
					stackCount = 0 ;
				}
				//Computer turn initiated
				initCompMove(); 
				//isHumanTurn = true; 
				
			}
			else {
				//TextView textView = (TextView) findViewById(R.id.t_note); 
				textView.setText("Invalid Move."); 
			}
		}
		/*else {
			//TextView textView = (TextView) findViewById(R.id.t_note); 
			//textView.setText("Invalid Move.");
		}*/
		
		Update_screen_data () ; 
		if (checkCondition()) {
			getNextHand();
			//return;
		}
	}
	
/** 
	 * Function Name: initSave
	   Purpose: Save the current data to a file 
       Parameters:  
	   Return Value: void
	   Local Variables:
	   		
Algorithm: 
	1) Read in the data from the stacks, hands etc. 
	2) Copy the data from the first file to the second file
	2)  save the data to the first file 
 Assistance Received: NONE 
	 * 
	 * 
	   Save the current data to a file
	   @param 
	   @return 
 */
	public void initSave (View view) {
		
		//Copy stuffs from MyFile.txt to MyFile2.txt; 
		commitToCopy(); 
		
		//Text to save in the file; 
		String text = "Computer:\nStacks: "+ compPlayer.toStringList(compPlayer.retStack()) + "\n";
		text += "Boneyard: " + compPlayer.toStringList(compPlayer.retBoneYard()) + "\n"; 
		text += "Hand: " + compPlayer.toStringList(compPlayer.retHand()) + "\n";
		text += "Score: "+ pd.retCompScore()+ "\n"; 
		text += "Rounds Won: " + pd.retCompRoundWon()+ "\n\n";
		//
		text += "Human:\nStacks: "+ humPlayer.toStringList(humPlayer.retStack()) + "\n";
		text += "Boneyard: " + humPlayer.toStringList(humPlayer.retBoneYard()) + "\n"; 
		text += "Hand: " + humPlayer.toStringList(humPlayer.retHand()) + "\n";
		text += "Score: "+ pd.retHumScore()+ "\n"; 
		text += "Rounds Won: " + pd.retHumRoundWon()+ "\n\n";
		//
		text += "Turn: "; 
		if (isHumanTurn) {
			text+= "Human"; 
		}
		else {
			text+= "Computer";
		}
		
		
		try {
			File logFile = new File(Environment.getExternalStorageDirectory().toString(), "myFile.txt");
			if(!logFile.exists()) {
			     logFile.createNewFile();
			}

			BufferedWriter output = new BufferedWriter(new FileWriter(logFile));
			output.write(text);
			output.close();
			
		} catch (Exception e) {
		        e.printStackTrace();
		}
		android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);    
		//android.os.Process.killProcess(android.os.Process.myPid());
	}
	
	/** 
	   When the user presses the pass button. Check if can be done. 
	   @param 
	   @return 
	   */
	public void setHumPass (View view) {
		humPass =true; 
		isHumanTurn = false; 
		if (compPlayer.compNextMove(compPlayer.retStack(), humPlayer.retHand(), true)) {
			TextView textView = (TextView) findViewById(R.id.t_note);
			String s1 = "Place Tile: " + compPlayer.toString(compPlayer.retChosenTile()) + " on Stack : "+ compPlayer.toString(compPlayer.retChosenStack()); 
			//String s2 = "Computer Turn";  
			textView.setText(s1);
			return; 
		}
		
		if (checkCondition()) {
			getNextHand();
			//return; 
		}
		initCompMove(); 
	}
	
	/** 
	   Call the CompNextMove with true as a parameter to use comp algorith to find the best move for the player 
	   @param 
	   @return 
	   */
	public void getHint (View view) {
		String reason = "\n"; 
		TextView textView = (TextView) findViewById(R.id.t_note);  
		if (compPlayer.compNextMove(compPlayer.retStack(), humPlayer.retHand(), true)) { 
			if (compPlayer.retChosenTile().left == compPlayer.retChosenTile().right) {
				reason += " It resets the opponents greatest tile using it's least double."; 
			}
			else {
				reason += " It resets the opponents greatest tile using it's least non-double.";
			}
			String s1 = "Place Tile: " + compPlayer.toString(compPlayer.retChosenTile()) + " on Stack : "+ compPlayer.toString(compPlayer.retChosenStack()) + reason;
			//String s2 = "Computer Turn";  
			textView.setText(s1);
		}
		else {
			textView.setText("pass");
		}
	}
		
/** 
	 * Function Name: Update_Screen_data
	   Purpose: To refresh the screen after every move 
       Parameters:  
	   Return Value: void
	   Local Variables:
	   		
Algorithm: 
	1) Gets the button clicked, and adds text based on the stack/hand num
	2) Uses the text to determine the background color
 Assistance Received: NONE 
	 * 
	 * 
	    Gets the button clicked, and adds text based on the stack/hand num
	   @param 
	   @return 
  */
	public void Update_screen_data () {
		/*TextView textView = (TextView) findViewById(R.id.t_turn); 
		String s1 = "Hum"; 
		String s2 = "Com"; 
		//isHumanTurn  ? textView.setText(s1) : textView.setText(s2); 
		if (isHumanTurn) {textView.setText(s1);}
		else {textView.setText(s2);} */
		
		//CompStack
		((Button) findViewById( R.id.compStack1 )).setText(compPlayer.toString(compPlayer.retStackInd(1)) );
		if (((Button) findViewById( R.id.compStack1 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.compStack1 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		else {
			((Button) findViewById( R.id.compStack1 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.compStack2 )).setText(compPlayer.toString(compPlayer.retStackInd(2)) );
		if (((Button) findViewById( R.id.compStack2 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.compStack2 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY); 
		}
		else {
			((Button) findViewById( R.id.compStack2 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.compStack3 )).setText(compPlayer.toString(compPlayer.retStackInd(3)) );
		if (((Button) findViewById( R.id.compStack3 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.compStack3 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY); 
		}
		else {
			((Button) findViewById( R.id.compStack3 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.compStack4 )).setText(compPlayer.toString(compPlayer.retStackInd(4)) );
		if (((Button) findViewById( R.id.compStack4 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.compStack4 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY); 
		}
		else {
			((Button) findViewById( R.id.compStack4 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.compStack5 )).setText(compPlayer.toString(compPlayer.retStackInd(5)) );
		if (((Button) findViewById( R.id.compStack5 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.compStack5 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY); 
		}
		else {
			((Button) findViewById( R.id.compStack5 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.compStack6 )).setText(compPlayer.toString(compPlayer.retStackInd(6)) );
		if (((Button) findViewById( R.id.compStack6 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.compStack6 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		else {
			((Button) findViewById( R.id.compStack6 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		//HumStack
		((Button) findViewById( R.id.humStack1 )).setText(humPlayer.toString(humPlayer.retStackInd(1)) );
		if (((Button) findViewById( R.id.humStack1 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.humStack1 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		else {
			((Button) findViewById( R.id.humStack1 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.humStack2 )).setText(humPlayer.toString(humPlayer.retStackInd(2)) );
		if (((Button) findViewById( R.id.humStack2 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.humStack2 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		else {
			((Button) findViewById( R.id.humStack2 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.humStack3 )).setText(humPlayer.toString(humPlayer.retStackInd(3)) );
		if (((Button) findViewById( R.id.humStack3 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.humStack3 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		else {
			((Button) findViewById( R.id.humStack3 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.humStack4 )).setText(humPlayer.toString(humPlayer.retStackInd(4)) );
		if (((Button) findViewById( R.id.humStack4 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.humStack4 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		else {
			((Button) findViewById( R.id.humStack4 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.humStack5 )).setText(humPlayer.toString(humPlayer.retStackInd(5)) );
		if (((Button) findViewById( R.id.humStack5 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.humStack5 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		else {
			((Button) findViewById( R.id.humStack5 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.humStack6 )).setText(humPlayer.toString(humPlayer.retStackInd(6)) );
		if (((Button) findViewById( R.id.humStack6 )).getText().toString().charAt(0) == 'B') {
			((Button) findViewById( R.id.humStack6 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		else {
			((Button) findViewById( R.id.humStack6 )).getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		}
		//HumHand
		((Button) findViewById( R.id.humHand1 )).setText(humPlayer.toString(humPlayer.retHandInd(1)) );
		if (!hasChosenHandTile) {
			((Button) findViewById( R.id.humHand1 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}	 	
		
		((Button) findViewById( R.id.humHand2 )).setText(humPlayer.toString(humPlayer.retHandInd(2)) );
		if (!hasChosenHandTile) {
			((Button) findViewById( R.id.humHand2 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.humHand3 )).setText(humPlayer.toString(humPlayer.retHandInd(3)) );
		if (!hasChosenHandTile) {
			((Button) findViewById( R.id.humHand3 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.humHand4 )).setText(humPlayer.toString(humPlayer.retHandInd(4)) );
		if (!hasChosenHandTile) {
			((Button) findViewById( R.id.humHand4 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.humHand5 )).setText(humPlayer.toString(humPlayer.retHandInd(5)) );
		if (!hasChosenHandTile) {
			((Button) findViewById( R.id.humHand5 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		((Button) findViewById( R.id.humHand6 )).setText(humPlayer.toString(humPlayer.retHandInd(6)) );
		if (!hasChosenHandTile) {
			((Button) findViewById( R.id.humHand6 )).getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		}
		//	
	}
	
	/** 
	 * Function Name: initCompMove
	   Purpose: calls respective functions to commit a move from computer 
       Parameters:  
	   Return Value: void
	   Local Variables:
	   		
Algorithm: 
	1) Compplayer's method returns true for the turn to be made,
	2) commit to the move, replacing the chosen stack with the chosen tile
 Assistance Received: NONE 
	 * 
	 * 
	   calls respective functions to commit a move from computer
	   @param 
	   @return 
  */
	public void initCompMove () {
		Tile[] oppStack = new Tile[6]; 
		oppStack =	humPlayer.retStack(); 
		int stackCount = 0; 
		boolean isInOppStack = false;
		String reason = "\n"; 
		if (compPlayer.compNextMove(oppStack, humPlayer.retHand(), false)) {
			humPass = false; 
			compPlayer.ReplaceTiles(); 
			for (int i = 0 ; i < 6; i++) {
				if (compPlayer.retChosenStack().equals(oppStack[i])) {
					isInOppStack = true; 
					stackCount = i ; 
					break; 
				}
			}
			if (isInOppStack) {
				humPlayer.setStackInd(compPlayer.retChosenTile(), stackCount+1);
			}
			else {
				Tile[] compStack = new Tile[6];
				compStack = compPlayer.retStack(); 
				for (int i = 0 ; i < 6; i++) {
					if (compPlayer.retChosenStack().equals(compStack[i])) {
						stackCount = i ; 
						break; 
					}
				}
				compPlayer.setStackInd(compPlayer.retChosenTile(), stackCount+1); // +1 b/c 
			}
			if (compPlayer.retChosenTile().left == compPlayer.retChosenTile().right) {
				reason += " It resets the opponents greatest tile using it's least double."; 
			}
			else {
				reason += " It resets the opponents greatest tile using it's least non-double.";
			}
			TextView textView = (TextView) findViewById(R.id.t_note); 
			String s1 = "Computer placed Tile: " + compPlayer.toString(compPlayer.retChosenTile()) + " on Stack : "+ compPlayer.toString(compPlayer.retChosenStack()) + reason; 
			//String s2 = "Computer passed it's turn";  
			textView.setText(s1); 
		}
		else {
			TextView textView = (TextView) findViewById(R.id.t_note);
			String s2 = "Computer passed it's turn";  
			textView.setText(s2); 
			compPass = true; 
		}
		/*if (compPass && humPass ||( compPlayer.retHandCounter() == 0 && humPlayer.retHandCounter() == 0) || (compPass && humPlayer.retHandCounter() == 0) || (humPass && compPlayer.retHandCounter() == 0)) {
			getNextHand();
		}*/
		isHumanTurn = true;
		if (checkCondition()) {
			getNextHand();
			//return; 
		}
	} 
	
	/** 
	   creates intent for the new screen to show score and start a new hand 
	   @param 
	   @return 
	   */	
	public void getNextHand () {
		//end of hand. Update pd. start a new Intent
		pd.SetCompHand(compPlayer.retHand()); 
		pd.SetCompStack(compPlayer.retStack());
		pd.SetHumHand(humPlayer.retHand()); 
		pd.SetHumStack(humPlayer.retStack());
		//
		pd.SetHumTurn(isHumanTurn);
		pd.SetHandNum((pd.retHandNum())+1);
		//
		pd.SetHumHandCounter(humPlayer.retHandCounter());
		pd.SetCompHandCounter(compPlayer.retHandCounter());
		//
		Intent intent = new Intent (this, Next_Hand.class);
		intent.putExtra("next_hand_screen", pd); 
		startActivity(intent);
		System.exit(1);
		
	}
	
	/** 
	   Called to copy data from first file to the second file
	   @param 
	   @return 
	   */	
	public void commitToCopy () {
		//puts the data from myFile to myFile2
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,"myFile.txt");
		//StringBuilder newLine = new StringBuilder();
		String newLine = "" ;
		try {
		    BufferedReader br = new BufferedReader(new FileReader(file));
		    String line; 
		    while ((line = br.readLine()) != null) {
		    	//newLine.append(line);
		    	//newLine.append("\n")
		    	newLine += line + "\n"; 
		    }
		    	br.close();
		    	
			}
			catch (IOException e) {
			    //You'll need to add proper error handling here
			}
		
		
		try {
			File logFile = new File(Environment.getExternalStorageDirectory().toString(), "myFile1.txt");
			if(!logFile.exists()) {
			     logFile.createNewFile();
			}

			BufferedWriter output = new BufferedWriter(new FileWriter(logFile));
			output.write(newLine);
			output.close();
			
		} catch (Exception e) {
		        e.printStackTrace();
		}
				
	}	
	public boolean checkCondition () {
		if (screenChangeSet) {
			return false; 
		}
		if (compPass && humPass ||( compPlayer.retHandCounter() == 0 && humPlayer.retHandCounter() == 0) || (compPass && humPlayer.retHandCounter() == 0) || (humPass && compPlayer.retHandCounter() == 0)) {
			screenChangeSet = true; 
			return true; 
		}
		else {
			return false; 
		}
	}
	public Tile toTile (String tileSt) {
		Tile temp = new Tile(); 
		temp.color =  tileSt.charAt(0);
		temp.left = Character.getNumericValue(tileSt.charAt(1));  
		temp.right = Character.getNumericValue(tileSt.charAt(2));
		temp.sum = temp.left + temp.right; 
		return temp; 
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.init__game, menu);
		return true;
	}
	@Override
	public void onBackPressed()
	{
		((Button) findViewById( R.id.b_save )).performClick();
	}

}
