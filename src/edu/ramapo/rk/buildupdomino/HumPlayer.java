package edu.ramapo.rk.buildupdomino;

public class HumPlayer extends Player {
	
	public HumPlayer (Parcel_Data pd) {
		boneYard = pd.retHumBoneYard();  
		hand = pd.retHumHand(); 
		stack = pd.retHumStack(); 
		//isHumanTurn = pd.retHumTurn();
		handCounter = pd.retHumHandCounter(); 
		//oppStack = pd.retCompStack(); 
	}
	
	public boolean ValidateMove () {
		boolean isDouble = false; 
		if (chosenTile.left == chosenTile.right) {isDouble = true;}
		if (isDouble && chosenStack.left != chosenStack.right){return true;}
		if (isDouble && (chosenStack.left == chosenStack.right) && chosenStack.sum <= chosenTile.sum ){return true;}
		if (chosenStack.sum <= chosenTile.sum){return true;}
		else {return false;}
	}
}
