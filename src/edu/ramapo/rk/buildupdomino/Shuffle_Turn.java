/**************************************************************
     * Name:  Rijesh Kumar                                      *
     * Project:  Project 3: BuildUp Domino - Android            *
     * Class:  OPL                                              *
     * Date:  11/15/2014                                        *
     ************************************************************/

package edu.ramapo.rk.buildupdomino;
import java.util.Random;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class Shuffle_Turn extends Activity {
	private Tile [] humBoneYard; 
	private Tile [] compBoneYard;
	private Tile[] humHand = new Tile[6];  
	private Tile[] compHand = new Tile[6]; 
	private Tile[] humStack = new Tile[6]; 
	private Tile[] compStack = new Tile[6];
	private Tile[] curHumBoneYard = new Tile[28]; 
	private Tile[] curCompBoneYard = new Tile[28]; 
	private boolean humTurn = false; 
	private Tile humFirstTile;
	private Tile compFirstTile; 
	private Parcel_Data pd = new Parcel_Data (); 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shuffle__turn);
		
		Intent intent=getIntent(); 
		pd = (Parcel_Data)intent.getSerializableExtra("to_shuffle_screen"); 
		humBoneYard = pd.retHumBoneYard(); 
		compBoneYard = pd.retCompBoneYard(); 
		for (int i = 0 ; i < 28 ; i++) {
			curHumBoneYard[i] = new Tile(); 
			curCompBoneYard[i] = new Tile(); 
		}
		
	}
	
	/** 
	 * Function Name: initGameScreen
	   Purpose: Create stacks, hands and make ready for the game to be played 
       Parameters:  
	   Return Value: void
	   Local Variables:
	   		
Algorithm: 
	1) Gets line from the file , using the while loop to stop
	2) Checks for various types : ex. Hand, Stack, BoneYard using Regular Expression
	3) Based on the type it updates the tiles to the players object.
	4) Creates an Intent to start the game 
 Assistance Received: NONE 
	 * 
	 * 
	   Create stacks, hands and make ready for the game to be played
	   @param 
	   @return 
	   */
	public void initGameScreen (View view) {
		if (listCounter(pd.retHumHand()) == 0) {
			humHand = createHand (humBoneYard);
			pd.SetHumHand(humHand); 
			createCurBoneYard (true);
			pd.SetHumHandCounter(6); 
		}
		if (listCounter(pd.retCompHand()) == 0) {
			compHand = createHand (compBoneYard);
			pd.SetCompHand(compHand);
			createCurBoneYard (false);
			pd.SetCompHandCounter(6);
		}
		if (listCounter(pd.retHumStack()) == 0) {
			humStack = createStack (humBoneYard);
			pd.SetHumStack(humStack);
			createCurBoneYard (true);
			
		}
		if (listCounter(pd.retCompStack()) == 0) {
			compStack = createStack (compBoneYard);
			pd.SetCompStack(compStack);
			createCurBoneYard (false);
		}
 
		Intent intent = new Intent (this, Init_Game.class);
		//
		pd.SetCompBoneYard(curCompBoneYard);
		pd.SetHumBoneYard(curHumBoneYard);
		pd.SetHumTurn(humTurn); 
		//
		intent.putExtra("pre_screen_data", pd); 
		startActivity(intent); 
		System.exit(1);
	}

	/** 
	   call functions to decide who gets the first turn
	   @param 
	   @return 
	   */	
	public void findTurn (View view) {  
		String turnDis = firstTurn() + "The first turn goes to "; 
		if (humTurn) {
			turnDis += "Human because humFirstTile: "+ humFirstTile.sum + " is greater than compFirstTile: "+ compFirstTile.sum ; 
			TextView textView = (TextView) findViewById(R.id.textView1);
		    textView.setTextSize(20);
		    textView.setText(turnDis); 
		}
		else {
			turnDis += "Computer because humFirstTile: "+ humFirstTile.sum + " is lesser than compFirstTile: "+ compFirstTile.sum ;
			TextView textView = (TextView) findViewById(R.id.textView1);
		    textView.setTextSize(20);
		    textView.setText(turnDis);
		}	
	}
	/** 
	   Create hand based on respective boneyard of the player
	   @param Tile[] BoneYard, hands to create based on  the list 
	   @return Tile [] list of tiles i.e hands
	   */	
	public Tile[] createHand (Tile[] BoneYard) {
		Tile[] temp = new Tile [6]; 
		for (int i = 0 ; i < 6 ; i ++ ) {
			temp[i] = BoneYard[i]; 
		}
		return temp; 
	}
	/** 
	   Create stack based on respective boneyard of the player
	   @param Tile[] BoneYard, stacks to create based on  the list 
	   @return Tile [] list of tiles i.e stacks
	   */
	public Tile[] createStack (Tile[] BoneYard) {
		Tile[] temp = new Tile [6];  
		for (int i = 0 ; i < 6 ; i ++ ) {
			temp[i] = BoneYard[i]; 
		}
		return temp;
	}
	/** 
	   removes the items from the list that has been used to create hands/stacks
	   @param Boolean isHumBy, checks from which boneyard to remove
	   @return 
	   */
	public void createCurBoneYard (boolean isHumBY) {
		int count = 0; 
		if (isHumBY) {
			for (int i = 6 ; i < humBoneYard.length ; i++) {
				curHumBoneYard[count++] =  humBoneYard[i]; 
			}
			humBoneYard = curHumBoneYard;
		}
		else {
			for (int i = 6 ; i < compBoneYard.length ; i++) {
				curCompBoneYard[count++] =  compBoneYard[i]; 
			}
			compBoneYard = curCompBoneYard;
		}
	}
	
	/** 
	   checks if the tiles have same sum, if true then shuffles to decide.
	   @param 
	   @return String output note, to say what happened 
	   */	
	public String firstTurn () {
		String outputNote = ""; 
		humFirstTile = humBoneYard[0]; 
		compFirstTile = compBoneYard[0]; 
		
		if (humFirstTile.sum == compFirstTile.sum) {
			humBoneYard = Shuffle(humBoneYard);
			compBoneYard = Shuffle(compBoneYard);
			outputNote += "Shuffled\n"; 
			firstTurn(); 
		}
		else if (humFirstTile.sum > compFirstTile.sum) {
			humTurn = true; 
		}
		else {
			humTurn = false; 
		}
		return outputNote; 
	} 
	public Tile[] Shuffle (Tile[] tiles) {
		Random rand = new Random (); 
		int shuffleLoc = 0; 
		for (int i = 0; i < listCounter(compBoneYard) ; i++ ) {
			shuffleLoc = rand.nextInt(listCounter(humBoneYard)); 
			Tile temp = tiles[i]; 
			tiles[i] = tiles[shuffleLoc]; 
			tiles[shuffleLoc] = temp; 
		}
		return tiles; 
	}
	
	public int listCounter (Tile[] aTile) {
		int count = 0; 
		Tile temp = new Tile (); 
		while (!aTile[count].equals(temp)) {
			count++; 
			if (count >= aTile.length) {
				break; 
			}
		}
		return count; 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.shuffle__turn, menu);
		return true;
	}

}
