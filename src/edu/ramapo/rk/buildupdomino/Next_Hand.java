/**************************************************************
     * Name:  Rijesh Kumar                                      *
     * Project:  Project 3: BuildUp Domino - Android            *
     * Class:  OPL                                              *
     * Date:  11/15/2014                                        *
     ************************************************************/

package edu.ramapo.rk.buildupdomino;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class Next_Hand extends Activity {
	private Parcel_Data pd = new Parcel_Data(); 
	private int humScore; 
	private int compScore; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_next__hand);
		
		Intent intent=getIntent(); 
		pd = (Parcel_Data)intent.getSerializableExtra("next_hand_screen"); 
		calcScore(); 
	}
	
	
	/** 
	   setup data for the next hand to start, and start the intent 
	   @param 
	   @return 
	   */	
	public void nextHandRes (View view) {
		//Create pd, with updated Score. create a new Intent to the Init_game screen
		// Create Hand.
		int numTilesInHand = 6 ; 
		if (pd.retHandNum() ==4) {
			numTilesInHand = 4; 
			pd.SetCompHandCounter(4); 
			pd.SetHumHandCounter(4); 
		}
		else if (pd.retHandNum() <4) {
			pd.SetCompHandCounter(6); 
			pd.SetHumHandCounter(6);
		}
		if (pd.retHandNum() >4) {
			// Init new Screen Intent
			pd.SetCompScore(compScore);
			pd.SetHumScore(humScore);
			Intent intent = new Intent (this, Round_Winner.class);
			intent.putExtra("end_round", pd); 
			startActivity(intent);
			System.exit(1);
			return; 
		}
		else {
			pd.SetCompHand(createHand (pd.retCompBoneYard()));
			pd.SetHumHand(createHand (pd.retHumBoneYard()));
			pd.SetCompBoneYard(resetBoneYard(pd.retCompBoneYard(), numTilesInHand ));
			pd.SetHumBoneYard(resetBoneYard(pd.retHumBoneYard(), numTilesInHand )); 
			//
			pd.SetCompScore(compScore);
			pd.SetHumScore(humScore);
			//start the Init_game intent
			Intent intent = new Intent (this, Init_Game.class);
			intent.putExtra("pre_screen_data", pd); 
			startActivity(intent);
			System.exit(1);
		}
	}
	
	/** 
	   Calculate the score based on tiles in the stacks
	   @param 
	   @return 
	   */
	public void calcScore () {
		humScore = pd.retHumScore(); 
		compScore = pd.retCompScore();
		//Tile[] humTile = new Tile[6]; 
		//Tile[] compTile = new Tile[6]; 

		for (int i = 0 ; i < 6 ; i++) {
			if (pd.retHumStack()[i].color == 'W') {
				compScore += pd.retHumStack()[i].sum;  
			}
			if (pd.retHumStack()[i].color == 'B') {
				humScore += pd.retHumStack()[i].sum;
			}
			if (pd.retCompStack()[i].color == 'W') {
				compScore += pd.retCompStack()[i].sum;
			}
			if (pd.retCompStack()[i].color == 'B') {
				humScore += pd.retCompStack()[i].sum;
			}
		}
		int count = 0; 
		Tile tmp = new Tile(); 
		while (! pd.retCompHand()[count].equals(tmp) ) {
			compScore -= pd.retCompHand()[count].sum; 
			count++; 
		}
		count = 0; 
		while (! pd.retHumHand()[count].equals(tmp) ) {
			humScore -= pd.retHumHand()[count].sum; 
			count++; 
		}
		TextView textView = (TextView)findViewById(R.id.humScore); 
		textView.setText(Integer.toString(humScore) ); 
		textView = (TextView)findViewById(R.id.compScore);
		textView.setText(Integer.toString(compScore) );
		
	}
	public Tile[] createHand (Tile[] BoneYard) {
		Tile[] temp = new Tile [6]; 
		for (int i = 0 ; i < 6 ; i ++ ) {
			temp[i] = BoneYard[i]; 
		}
		/*for (int i = 0 ; i < 6 ; i ++ ) {
			removeElement(BoneYard, 0); 
		}*/
		return temp; 
	}
	
	
	
	public Tile[] resetBoneYard (Tile[] bYard, int val) {
		
		int count = val;
		Tile[] temp = new Tile[bYard.length]; 
		for (int i = 0 ; i < temp.length ; i++) {
			temp[i] = new Tile (); 	
		}
		for (int i = 0 ; i < bYard.length - val; i ++) {
			temp[i] = bYard[count++]; 
		}
		return temp; 
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.next__hand, menu);
		return true;
	}

}
