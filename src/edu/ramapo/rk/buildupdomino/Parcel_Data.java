package edu.ramapo.rk.buildupdomino;

import java.io.Serializable;

public class Parcel_Data implements Serializable{
	
	/**
	 * Responsible for setting up data which is used to transfer data from one activity to another
	 */
	private static final long serialVersionUID = 185029536320062788L;
	private Tile [] humBoneYard = new Tile[28];
	private Tile [] compBoneYard = new Tile[28];
	private Tile[] humHand = new Tile[6];  
	private Tile[] compHand = new Tile[6]; 
	private Tile[] humStack = new Tile[6]; 
	private Tile[] compStack = new Tile[6];
	private boolean humTurn = false;  
	private int handNum = 1 ; 
	private int humScore = 0 ;
	private int compScore = 0 ;
	private int humRoundWon = 0;
	private int compRoundWon = 0;
	private int humHandCounter = 6;
	private int compHandCounter = 6; 
	
	public void resetValues () {
		Tile temp = new Tile();
		for (int i = 0 ; i < 28 ; i++) {
			humBoneYard[i] = temp;
			compBoneYard[i] = temp; 
			if (i < 6) {
				compHand[i] = temp; 
				humHand[i] = temp; 
				compStack[i] = temp;
				humStack[i] = temp; 
			}
		}
		humTurn = false;  
		handNum = 1 ; 
		humScore = 0 ;
		compScore = 0 ;
		humRoundWon = 0;
		compRoundWon = 0;
		humHandCounter = 6;
		compHandCounter = 6;
		
	}
	
	public int retHumHandCounter () {
		return humHandCounter; 
	}
	public int retCompHandCounter () {
		return compHandCounter; 
	}
	public void SetCompHandCounter (int val) {
		compHandCounter = val; 
	}
	public void SetHumHandCounter (int val) {
		humHandCounter = val; 
	}
	
	public void SetHumHand (Tile[] hand) {
		humHand = hand; 
	}
	
	public Tile[] retHumHand () {
		return humHand; 
	}
	
	public void SetCompHand (Tile[] hand) {
		compHand = hand; 
	}
	
	public Tile[] retCompHand () {
		return compHand; 
	}
	public void SetHumStack (Tile[] stack) {
		humStack = stack; 
	}
	
	public Tile[] retHumStack () {
		return humStack; 
	}
	public void SetCompStack (Tile[] stack) {
		compStack = stack; 
	}
	
	public Tile[] retCompStack () {
		return compStack; 
	}
	public void SetCompBoneYard (Tile[] bYard) {
		compBoneYard = bYard; 
	}
	
	public Tile[] retCompBoneYard () {
		return compBoneYard; 
	}
	public void SetHumBoneYard (Tile[] bYard) {
		humBoneYard = bYard; 
	}
	
	public Tile[] retHumBoneYard () {
		return humBoneYard; 
	}
	public void SetHumTurn (boolean isHumTurn) {
		humTurn = isHumTurn;	
	}
	public boolean retHumTurn () {
		return humTurn; 
	}
	
	public void SetHandNum (int val) {
		handNum = val; 
	}
	
	public int retHandNum () {
		return handNum;
	}
	
	public void SetHumScore (int val) {
		humScore = val; 
	}
	
	public int retHumScore () {
		return humScore;
	}
	public void SetCompScore (int val) {
		compScore = val; 
	}
	
	public int retCompScore () {
		return compScore;
	}
	
	public void SetHumRoundWon (int val) {
		humRoundWon = val; 
	}
	
	public int retHumRoundWon () {
		return humRoundWon;
	}
	public void SetCompRoundWon (int val) {
		compRoundWon = val; 
	}
	
	public int retCompRoundWon () {
		return compRoundWon;
	}
	public String toString (Tile aTile) {
		String temp= "";
		temp += aTile.color + Integer.toString(aTile.left) + Integer.toString(aTile.right); 
		return temp;
	}
	
}
