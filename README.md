BUILD-UP DOMINO:

USER MANUAL

Build up Domino which is designed in Android creates a very user friendly environment for the user to play against the computer. 

HOW TO RUN:  Once the game starts the screen comes up with the option to load from the previous game or start a new game. The user has the option to save upto 3 games, and anyone at anytime. 
Once the user selects load or Play a new Game, the game resumes. The user can place his/her tile on the opponents stack, or his own stack.  The following are the features provided: 
a.	Help: The user has the option to seek help. The help returns with the most viable next move for you (press “help ” when asked to play your turn). 
b.	Save: The user can save the game at any point to be played again later. (click “save”). 
c.	Load: The user can resume playing an earlier saved game. (click “load” when prompted).

The user friendly environment makes sure that the user knows what are the options for his/her move. The screen shows the user with the tiles in stack of the computer. It shows the tiles in the hand and stack of the human player too. Along with that, the UI also shows the Next turn of the player, current score, and rounds won by each player. Furthermore, the tiles are colored according to the player it belongs to. It changes it color, when user selects a tile. 

BUG REPORT: 
The followings are the bugs associated with the program: 
	No bugs found yet. Users are encouraged to give their suggestions. 
Features: All basic features are implemented, with regards to the functionality of the game.  
